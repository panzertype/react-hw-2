import React from 'react';
import Logo from '../Header/components/Logo/Logo';
import Button from '../../common/Button/Button';
import { useNavigate, useLocation } from 'react-router-dom';

const Header = () => {
	const navigate = useNavigate();
	const location = useLocation();
	const hideData = ['/login', '/register'];

	const token = localStorage.getItem('token');
	const user = JSON.parse(localStorage.getItem('user'));

	return (
		<nav className='border border-danger border-2 navbar px-4 py-1'>
			<Logo />
			{token && user.name && !hideData.some((p) => p === location.pathname) ? (
				<ul className='navbar-nav flex-row gap-3 ms-auto'>
					<li className='align-self-center'>{user.name}</li>
					<li>
						<Button
							onClick={() => {
								localStorage.removeItem('token');
								localStorage.removeItem('user');
								navigate('/login');
							}}
							title='Logout'
						/>
					</li>
				</ul>
			) : (
				''
			)}
		</nav>
	);
};

export default Header;
