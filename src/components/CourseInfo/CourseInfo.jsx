import React from 'react';
import { Link, useParams } from 'react-router-dom';
import { mockedCoursesList, mockedAuthorsList } from '../../constants';
import getTime from '../../helpers/pipeDuration';

const CourseInfo = () => {
	const { courseId } = useParams();

	const course = mockedCoursesList.find((b) => b.id === courseId);

	const getAuthors = (authors) => {
		const arr = [];
		authors.forEach((a) => {
			const foundIndex = mockedAuthorsList.find((b) => b.id === a);
			if (foundIndex) {
				arr.push(foundIndex.name);
			}
		});
		return arr;
	};

	return (
		<div className='border border-info border-2 p-4'>
			<Link to='/courses'>&lt; Back to courses</Link>
			<h2 className='text-center'>{course.title}</h2>
			<div className='row'>
				<div className='col-md-8'>{course.description}</div>
				<div className='col-md-4'>
					<div>
						<span className='fw-bold'>ID: </span>
						{course.id}
					</div>
					<div>
						<span className='fw-bold'>Duration: </span>
						{getTime(course.duration)} hours
					</div>
					<div>
						<span className='fw-bold'>Created: </span>
						{course.creationDate}
					</div>
					<div>
						<span className='fw-bold'>Authors: </span>
						{getAuthors(course.authors).join(', ')}
					</div>
				</div>
			</div>
		</div>
	);
};

export default CourseInfo;
