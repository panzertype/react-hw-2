import React, { useState, useMemo } from 'react';
import { useNavigate } from 'react-router-dom';
import CourseCard from './components/CourseCard/CourseCard';
import SearchBar from './components/SearchBar/SearchBar';
import { mockedCoursesList, mockedAuthorsList } from '../../constants';

const Courses = (props) => {
	const navigate = useNavigate();

	const [courses] = useState(mockedCoursesList);
	const [searchValue, setSearchValue] = useState('');
	const [searchQuerry, setSearchQuerry] = useState('');

	const searchedCourses = useMemo(() => {
		return courses.filter(
			(course) =>
				course.title.toLowerCase().includes(searchQuerry.toLowerCase()) ||
				course.id.toLowerCase().includes(searchQuerry.toLowerCase())
		);
	}, [searchQuerry, courses]);

	const getAuthors = (authors) => {
		const arr = [];
		authors.forEach((a) => {
			const foundIndex = mockedAuthorsList.find((b) => b.id === a);
			if (foundIndex) {
				arr.push(foundIndex.name);
			}
		});
		return arr;
	};

	return (
		<div className='border border-info border-2 d-flex flex-column gap-4 p-4'>
			<SearchBar
				type='text'
				onChange={(event) => {
					if (event.target.value.length > 0) {
						setSearchValue(event.target.value);
					} else {
						setSearchValue('');
						setSearchQuerry('');
					}
				}}
				onCreate={() => {
					navigate('/courses/add');
					props.onCreate();
				}}
				onClick={() => setSearchQuerry(searchValue)}
				labelText='search'
				placeholderText='Enter course name or id...'
			/>
			{searchedCourses.map((post, index) => (
				<CourseCard
					key={post.id}
					post={post}
					getAuthors={getAuthors}
					getTime={props.getTime}
				/>
			))}
		</div>
	);
};

export default Courses;
