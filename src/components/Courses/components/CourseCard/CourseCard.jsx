import React from 'react';
import Button from '../../../../common/Button/Button';
import { useNavigate } from 'react-router-dom';

const CourseCard = (props) => {
	const navigate = useNavigate();

	return (
		<div className='border border-success border-2 p-4'>
			<div className='row'>
				<div className='col-md-8'>
					<h2 className='mb-3'>{props.post.title}</h2>
					<p>{props.post.description}</p>
				</div>
				<div className='col-md-4'>
					<div
						style={{
							textOverflow: 'ellipsis',
							overflow: 'hidden',
							whiteSpace: 'nowrap',
						}}
					>
						<span className='fw-bold'>Authors: </span>
						{props.getAuthors(props.post.authors).join(', ')}
					</div>
					<div>
						<span className='fw-bold'>Duration: </span>
						{props.getTime(props.post.duration)} hours
					</div>
					<div>
						<span className='fw-bold'>Created: </span>
						{props.post.creationDate}
					</div>
					<Button
						onClick={() => navigate('/courses/' + props.post.id)}
						style={{ margin: '16px auto auto auto', display: 'block' }}
						title='Show course'
					/>
				</div>
			</div>
		</div>
	);
};

export default CourseCard;
